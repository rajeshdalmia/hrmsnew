import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hrms';
  // dtOptions: DataTables.Settings = {};


  constructor(private cookie : CookieService) { }

  ngOnInit() {

    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 5,
    //   processing: true
    // };

    // console.log(this.cookie.get("Employee"));
    var abc = JSON.parse(localStorage.getItem("Employee"));
    //console.log(abc);
    return abc;
    }

  isUserLoggedIn() {
      let user = localStorage.getItem('Employee')
      // console.log(user)
     return !(user === null)
  }



}

