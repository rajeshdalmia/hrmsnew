import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { userInterface } from '../admin/user/userInterface';
import { moduleInterface } from '../admin/module/moduleInterface';
import { roleInterface } from '../admin/role/roleInterface';
import { companyInterface } from '../admin/company/companyInterface';

@Injectable({
  providedIn: 'root'
})

export class ApiService { 

 // apiURL = 'http://localhost:3000/employees/';
   apiURL = 'http://localhost/laravel/hrms/laravel/public/api/v1/';

  constructor(private http: HttpClient) { }

// Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  redirectUrl: string;
  
  isLoggedIn() {
    if (localStorage.getItem('Employee')) {
      return true;
    }
    return false;
  }

/* *Module */

  getModules(): Observable<moduleInterface> {
    return this.http.get<moduleInterface>(this.apiURL + 'module')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getModulesActive(): Observable<moduleInterface> {
    return this.http.get<moduleInterface>(this.apiURL + 'module-active')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getModule(id): Observable<moduleInterface> {
    return this.http.get<moduleInterface>(this.apiURL + 'module/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  createModule(data): Observable<moduleInterface> {
    return this.http.post<moduleInterface>(this.apiURL + 'module', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  updateModule(id, data): Observable<moduleInterface> {
    return this.http.put<moduleInterface>(this.apiURL + 'module/' + id, JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  updateStatusModule(id){
    return this.http.put<moduleInterface>(this.apiURL + 'module-status/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  
  /* * Role */

 getRoles(): Observable<roleInterface> {
  return this.http.get<roleInterface>(this.apiURL + 'role')
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

getRole(id): Observable<roleInterface> {
  return this.http.get<roleInterface>(this.apiURL + 'role/' + id)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}  

createRole(data): Observable<roleInterface> {
  return this.http.post<roleInterface>(this.apiURL + 'role', JSON.stringify(data), this.httpOptions)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}  

updateRole(id, data): Observable<roleInterface> {
  return this.http.put<roleInterface>(this.apiURL + 'role/' + id, JSON.stringify(data), this.httpOptions)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

updateStatusRole(id){
  return this.http.put<roleInterface>(this.apiURL + 'role-status/' + id, this.httpOptions)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}


  /* * User */

  getEmployees(): Observable<userInterface> {
    return this.http.get<userInterface>(this.apiURL + 'user')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method => Fetch employee
  getEmployee(id): Observable<userInterface> {
    return this.http.get<userInterface>(this.apiURL + 'user/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method => Create employee
  createUser(data): Observable<userInterface> {
    return this.http.post<userInterface>(this.apiURL + 'user', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API put() method => Update employee
  updateEmployee(id, employee): Observable<userInterface> {
    return this.http.put<userInterface>(this.apiURL + 'user/' + id, JSON.stringify(employee), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method => Delete employee
  deleteEmployee(id){
    return this.http.delete<userInterface>(this.apiURL + 'user/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  activeEmployee(id){
    return this.http.put<userInterface>(this.apiURL + 'user-active/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  inactiveEmployee(id){
    return this.http.put<userInterface>(this.apiURL + 'user-inactive/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }

  getVerticleData(){
    return this.http.get<userInterface[]>(this.apiURL + 'user')
  }

//--company--//

  getCompanyData(){
    return this.http.get<companyInterface[]>(this.apiURL + 'company');
  }
  
  createCompany(data):Observable<companyInterface>{
     return this.http.post<companyInterface>(this.apiURL + 'company', JSON.stringify(data),this.httpOptions)
     .pipe(
         retry(1),
         catchError(this.handleError)
       ) 
  }

  activeCompany(id){
    return this.http.put<companyInterface>(this.apiURL + 'company-active/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  inactiveCompany(id){
    return this.http.put<companyInterface>(this.apiURL + 'company-inactive/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }


}
