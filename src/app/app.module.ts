import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { AppComponent } from './app.component';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {NgxPaginationModule} from 'ngx-pagination';

import { SidebarComponent } from './admin/sidebar/sidebar.component';

import { AppRoutingModule, routingComponent } from './app-routing.module';
import { HeaderComponent } from './admin/header/header.component';
import { FooterComponent } from './admin/footer/footer.component';

import { AuthGuard } from './auth/auth.guard';
import { AddCompanyComponent } from './admin/company/add-company/add-company.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AddCompanyDocsComponent } from './admin/company/add-company-docs/add-company-docs.component';
import { CompanyReferenceComponent } from './admin/company-reference/company-reference.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TestComponent } from './admin/test/test.component';

@NgModule({
  declarations: [
    AppComponent,
     routingComponent, HeaderComponent,   FooterComponent,    SidebarComponent,
      AddCompanyComponent, DashboardComponent, AddCompanyDocsComponent, 
      CompanyReferenceComponent, TestComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,NgxPaginationModule,FilterPipeModule,
    AppRoutingModule,FormsModule, ReactiveFormsModule, Ng2SearchPipeModule,
     NgbModule.forRoot()
  ],
  providers: [CookieService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
