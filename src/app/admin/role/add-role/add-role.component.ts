import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-add-role.component',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.css']
})

export class AddRoleComponent implements OnInit {

  datamodule: any =[];

    selectedModule : any = [];

  @Input() details = { 
    name: '', description: '', assign_modules: ''
 }
  
   constructor(public restApi: ApiService, public router: Router) { }

  ngOnInit() {
    this.restApi.getModulesActive().subscribe((datamodule: {}) => {
      this.datamodule = datamodule;
    })
   }

  createRole(dataPost) {
    this.details.assign_modules = this.selectedModule.toString();
    //console.log(this.selectedModule.toString());
    this.restApi.createRole(this.details).subscribe((data: {}) => {
      this.router.navigate(['/role'])
    })
  }

    // on multiple checkbox selection
    moduleChange(event){
      let index = this.selectedModule.indexOf(event.target.value);
      if(index == -1){
        this.selectedModule.push(event.target.value);
      }
    else {
        this.selectedModule.splice(index, 1);
      }
  
    }

    
}