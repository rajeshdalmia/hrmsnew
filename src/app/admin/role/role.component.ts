import { Component, OnInit } from '@angular/core';
import { roleInterface } from './roleInterface';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-module',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  items : roleInterface;

  constructor( private apiService : ApiService) { }

  ngOnInit() {
    this.getRoles()
  }

  getRoles() {
    return this.apiService.getRoles().subscribe(data => this.items = data)    
  }


  updateStatusRole(id) {
    this.apiService.updateStatusRole(id).subscribe(data => {
      this.getRoles()
    })
    
  }  


}
