import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-edit-module',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.css']
})
export class EditRoleComponent implements OnInit {

  Error= false;
  message: string;
  id = this.actRoute.snapshot.params['id'];
  data: any = {};
  datamodule: any = [];
  selectedModule : any = [];

  constructor(
    public restApi: ApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit() {
    // get Role data
    this.restApi.getRole(this.id).subscribe((data: {}) => {
      this.data = data;     
    })
  
    // get Active Module data
    this.restApi.getModulesActive().subscribe((datamodule: {}) => {
      this.datamodule = datamodule;
      
    //  this.selectedModule = this.data.assign_modules.split(",");
    //  console.log(this.selectedModule);

    })
  }

  updateRole() {
    this.data.assign_modules = this.selectedModule.toString();
      this.restApi.updateRole(this.id, this.data).subscribe(data => {
      if (data.status == '0') 
      {
        this.Error = true;
        this.message = data.message;
      }else {
        this.router.navigate(['/role'])
      } 
        
    })
  
  }

  // on multiple checkbox selection
  moduleChange(event){
    let index = this.selectedModule.indexOf(event.target.value);
    if(index == -1){
      this.selectedModule.push(event.target.value);
     // console.log('selected value')
    }
  else {
      this.selectedModule.splice(index, 1);
    }

  }


}