import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor( private authService : ApiService) { }

  
  ngOnInit() {
    
  }

  logout(){
    localStorage.removeItem('Employee');  
  }

  get isLoggedIn() { return this.authService.isLoggedIn(); }

}
