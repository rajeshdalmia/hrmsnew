import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { AuthGuard } from '../auth/auth.guard';


const adminRoutes: Routes = [
   { path: 'user', component: UserComponent, canActivate : [AuthGuard],
        children: [
          {
            path: '',
            canActivateChild: [AuthGuard],
            children: [
            {  path: 'add-user', component: AddUserComponent  },
            {  path: ':id', component: EditUserComponent  }
        ]
      } 
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
