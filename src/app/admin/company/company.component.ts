import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { companyInterface } from './companyInterface';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  companyinfo : companyInterface[];

  constructor(private apiService : ApiService) { }

  ngOnInit() {
      this.loadCompanyData();
  }

  loadCompanyData(){
    return this.apiService.getCompanyData().subscribe(data => this.companyinfo = data);
  }

  activeCompany(id){
     if(window.confirm('Are you sure, you want to change inactive')){
       this.apiService.activeCompany(id).subscribe(data =>{
         this.loadCompanyData();
       })
     }
  }

  inactiveCompany(id){
    if(window.confirm('Are you sure, you want to change active ?')){
      this.apiService.inactiveCompany(id).subscribe(data =>{
        this.loadCompanyData();
      })
    }
  }

  
}
