import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { Router } from '@angular/router';
import { companyInterface } from '../companyInterface';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css']
})
export class AddCompanyComponent implements OnInit {

  //companyInfo : companyInterface[];

  @Input() details = { 
    name: '', email: '', password: '', meta_description : '',
    cstatus : '', created_at : '',  updated_at : ''
 }
  
   constructor(public restApi: ApiService, public router: Router) { }

  ngOnInit() { }
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$"; 

  pinCodePattern = "[1-9][0-9]{5}$"; 

  state : any = ['Delhi', 'Uttar Pradesh', 'Uttarakhand'];

  create(dataEmployee) {
    this.restApi.createCompany(this.details).subscribe((data : {}) =>{
      this.router.navigate(['/company']);
    })   
  }


}
