export class companyInterface {
    id: number;   
    company_name : string;
    company_logo : boolean;
    company_address : string;
    company_city : string;
    company_state: string; 
    company_pincode : number;
    company_contact_number : number;
    company_email : string;
    cstatus : boolean;  
}
