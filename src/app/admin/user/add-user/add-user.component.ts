import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-add-user.component',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})

export class AddUserComponent implements OnInit {

  @Input() details = { 
    name: '', email: '', password: '', meta_description : '',
    cstatus : '', created_at : '',  updated_at : ''
 }
  
   constructor(public restApi: ApiService, public router: Router) { }

  ngOnInit() { }

  create(dataEmployee) {
    this.restApi.createUser(this.details).subscribe((data: {}) => {
      this.router.navigate(['/user'])
    })
  }

}