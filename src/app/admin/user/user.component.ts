import { Component, OnInit } from '@angular/core';
//import { EmplyeeService } from 'src/app/auth/employeeservice';
import { userInterface } from './userInterface';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

   user1 : userInterface[];
   searchText;

  constructor( private apiService : ApiService) { }

  ngOnInit() {
    this.loadEmployees();
   
  }

  loadEmployees() {
    return this.apiService.getVerticleData().subscribe(data => this.user1 = data);
     
  }

  deleteEmployee(id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.apiService.deleteEmployee(id).subscribe(data => {
        this.loadEmployees()
      })
    }
  }  

  activeEmployee(id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.apiService.activeEmployee(id).subscribe(data => {
        this.loadEmployees()
      })
    }
  }  

  inactiveEmployee(id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.apiService.inactiveEmployee(id).subscribe(data => {
        this.loadEmployees()
      })
    }
  } 




}