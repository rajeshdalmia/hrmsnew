import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-module',
  templateUrl: './edit-module.component.html',
  styleUrls: ['./edit-module.component.css']
})
export class EditModuleComponent implements OnInit {

  Error= false;
  message: string;
  id = this.actRoute.snapshot.params['id'];
  data: any = {};
  // public apistatus: boolean;

  constructor(
    public restApi: ApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit() {
    this.restApi.getModule(this.id).subscribe((data: {}) => {
      this.data = data;
    })
  }

  // Update employee data
  updateModule() {

      this.restApi.updateModule(this.id, this.data).subscribe(data => {
        
        // return data.status;
    if (data.status == '0') 
    {
      this.Error = true;
      // this.message = 'errorrrrrrrrrrrr..';//data.message;
      this.message = data.message;
      // console.log(data.status);
      // console.log('iiiiiiiiiiii');
      // this.router.navigate(['/module'])
    }else {
      this.router.navigate(['/module'])
    } 

        
        
      })
  
  }

}
