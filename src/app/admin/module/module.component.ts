import { Component, OnInit } from '@angular/core';
import { moduleInterface } from './moduleInterface';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})
export class ModuleComponent implements OnInit {

  items : moduleInterface;

  constructor( private apiService : ApiService) { }

  ngOnInit() {
    this.getModules()
  }

  getModules() {
    return this.apiService.getModules().subscribe(data => this.items = data)    
  }


  updateStatusModule(id) {
    this.apiService.updateStatusModule(id).subscribe(data => {
      this.getModules()
    })
    
  }  


}
