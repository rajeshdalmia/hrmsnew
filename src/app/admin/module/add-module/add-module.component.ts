import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-add-module.component',
  templateUrl: './add-module.component.html',
  styleUrls: ['./add-module.component.css']
})

export class AddModuleComponent implements OnInit {

  @Input() details = { 
    module_code: '', module_name: ''
 }
  
   constructor(public restApi: ApiService, public router: Router) { }

  ngOnInit() { }

  createModule(dataPost) {
    this.restApi.createModule(this.details).subscribe((data: {}) => {
      this.router.navigate(['/module'])
    })
  }

}