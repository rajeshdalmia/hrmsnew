import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-company-reference',
  templateUrl: './company-reference.component.html',
  styleUrls: ['./company-reference.component.css']
})
export class CompanyReferenceComponent implements OnInit {


  showModal():void {
    $("#myModal").modal('show');
  }
  sendModal(): void {
    this.hideModal();
  }
  hideModal():void {
    document.getElementById('close-modal').click();
  }

  ngOnInit() {
  }

}
