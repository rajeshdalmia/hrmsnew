import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Employee} from './employee';
import { Loginemployee } from './login/loginemployee';
import { userInterface } from '../admin/user/userInterface';


@Injectable({
  providedIn: 'root'
})

export class EmplyeeService {
  url= 'http://localhost/laravel/hrms/laravel/public/api/v1/';

  constructor(private http:HttpClient) { }
 
  createemployee(employee:Employee):Observable<Employee>{
    return this.http.post<Employee>(this.url+'user',employee)
  }

  loginemployee(loginEmployee:Loginemployee):Observable<Employee> {
    return this.http.post<Employee>(this.url + 'login', loginEmployee)
  }


  // getVerticleData(){
  //   return this.http.get<userInterface[]>(this.url + 'user')
  // }



}