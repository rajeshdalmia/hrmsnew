import { Component, OnInit } from '@angular/core';
import {FormBuilder,Validators,FormGroup} from '@angular/forms';
import { Loginemployee } from './loginemployee';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { EmplyeeService } from '../employeeservice';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  

  loginForm: FormGroup;
  message: string;
  Error = false;
  constructor( private employeeservice: EmplyeeService, private formbuilder:FormBuilder,
    private router:Router, private cookie : CookieService) { }

  ngOnInit() {
    console.log('login');
    this.setFormState();  
    
  }
  setFormState(): void {
    this.loginForm = this.formbuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  onSubmit(){   
    let login=this.loginForm.value;
    this.login(login);
  }
  
  login(loginEmployee: Loginemployee) {
    this.employeeservice.loginemployee(loginEmployee).subscribe(
      employee => {
        // debugger;
       
        if (employee.status == '1') 
        {
          this.loginForm.reset();
          localStorage.setItem("Employee", JSON.stringify(employee));
          this.router.navigate(['/dashboard']);   
        }else {
          this.Error = true;
          this.message = employee.message;

        }  
      }
    )

  }

 

}