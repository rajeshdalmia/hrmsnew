import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { UserComponent } from './admin/user/user.component';
import { AddUserComponent } from './admin/user/add-user/add-user.component';
import { EditUserComponent } from './admin/user/edit-user/edit-user.component';

import { ModuleComponent } from './admin/module/module.component';
import { AuthGuard } from './auth/auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AddModuleComponent } from './admin/module/add-module/add-module.component';
import { EditModuleComponent } from './admin/module/edit-module/edit-module.component';
import { RoleComponent } from './admin/role/role.component';
import { CompanyComponent } from './admin/company/company.component';
import { AddRoleComponent } from './admin/role/add-role/add-role.component';
import { EditRoleComponent } from './admin/role/edit-role/edit-role.component';

import { EmployeeComponent } from './admin/employee/employee.component';
import { AssignRoleComponent } from './admin/assign-role/assign-role.component';
import { AssignModuleComponent } from './admin/assign-module/assign-module.component';
import { CompanyReferenceComponent } from './admin/company-reference/company-reference.component';
import { AddCompanyComponent } from './admin/company/add-company/add-company.component';
import { AddCompanyDocsComponent } from './admin/company/add-company-docs/add-company-docs.component';
import { TestComponent } from './admin/test/test.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {path : 'login', component : LoginComponent},
  {path : 'register', component : RegisterComponent},
  { path : 'forgotPassword', component :ForgotPasswordComponent}, 

  
  {path : 'dashboard', component : DashboardComponent, canActivate: [AuthGuard]},
   
  {path : 'module', component : ModuleComponent, canActivate: [AuthGuard]} ,
  {path : 'add-module', component : AddModuleComponent, canActivate: [AuthGuard]},
  {path : 'edit-module/:id', component : EditModuleComponent, canActivate: [AuthGuard]},

  {path : 'role', component : RoleComponent, canActivate: [AuthGuard]} ,
  {path : 'add-role', component : AddRoleComponent, canActivate: [AuthGuard]},
  {path : 'edit-role/:id', component : EditRoleComponent, canActivate: [AuthGuard]},
 
  {path: 'user', component: UserComponent, canActivate: [AuthGuard]}, 
  {path : 'add-user', component : AddUserComponent, canActivate: [AuthGuard] },
  {path: 'edit-user/:id', component: EditUserComponent, canActivate: [AuthGuard] } , 
  

  {path : 'company', component : CompanyComponent, canActivate: [AuthGuard] },
  {path : 'add-company', component : AddCompanyComponent, canActivate: [AuthGuard] },
  {path: 'add-company-step2', component: AddCompanyDocsComponent, canActivate: [AuthGuard]}, 
  {path: 'company-reference', component: CompanyReferenceComponent, canActivate: [AuthGuard]}, 
  {path: 'testing', component: TestComponent}, 
  
  { path: '**', component: PageNotFoundComponent },
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponent = [
  DashboardComponent, ForgotPasswordComponent,LoginComponent,RegisterComponent,
  ModuleComponent,RoleComponent,CompanyComponent,UserComponent,EditRoleComponent,
  EmployeeComponent, AssignRoleComponent, AssignModuleComponent,AddCompanyComponent,
  CompanyReferenceComponent, AddUserComponent, EditUserComponent,AddRoleComponent,
  PageNotFoundComponent, AddModuleComponent,EditModuleComponent, TestComponent


 

]